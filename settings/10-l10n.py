# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'America/Montreal'
USE_TZ = True
USE_I18N = True
USE_L10N = True
LANGUAGE_CODE = 'fr_CA'
LANGUAGES = (
    ('en', 'English'),
    ('fr', 'French'),
)
